package com.example.formulario_chungo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText etFirstNumber;
    EditText etSecondNumber;
    TextView tvMayor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {

        etFirstNumber = findViewById (R.id.etFirstNumber);
        etSecondNumber = findViewById (R.id.etSecondNumber);
        tvMayor = findViewById (R.id.tvMayor);

        int firstNumber = Integer.parseInt(etFirstNumber.getText().toString());
        int secondNumber = Integer.parseInt(etSecondNumber.getText().toString());

        int mayor = (firstNumber>secondNumber)? firstNumber: secondNumber;

        tvMayor.setText(mayor+"");






    }
}
